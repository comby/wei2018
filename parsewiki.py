#!/usr/bin/env python3
# -*- coding:utf-8 -*

"""Pour récupérer des infos à partir du wiki."""

import time
import re
#import urllib.request
import pprint

#: URL de la page wiki des bus
URL = "https://wiki.crans.org/OrganisationWei{}/Bus?action=raw".format(
    time.localtime()[0])
FILE = "rawpage"


def get_raw():
    """Récupère le contenu de la page."""
    return open(FILE).read()
    # page = urllib.request.urlopen(URL)
    # text = page.read().decode("utf-8")
    return text


def parse_names(raw):
    """Récupère les tableaux de Bus (= un tableau après un titre commençant par '== Bus')."""
    names = []
    l = re.findall("== (?:Bi)?Bus.*==[^|]*\n((?:\|\|.*\|\|\n)+)", raw)
    for table in l:
        obj = re.search("Nom de l'équipe[^|]*\|\|", table)
        after = table[obj.end():]
        end = after.index("\n")
        after = after[:end]
        teams = re.findall("([^|]+)\|\|", after)
        teams = [t.strip() for t in teams]
        names.append(teams)
    return names


def get_team_names():
    raw = get_raw()
    teams = parse_names(raw)
    return teams


if __name__ == "__main__":
    for t in get_team_names():
        print(t)
