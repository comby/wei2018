#!/usr/bin/env python3
# -*- coding:utf8 -*

"""
Script pour le remplissages des 1A dans les bus du WEI

TODO :
- adapter parse_wiki
- géneration structure donnée
- implémentation algorithme

"""

import sys
import csv
from collections import OrderedDict
import copy
from unA import *
from bus import *
import parsewiki
import algorithms
import random
import json

def get_1A_from_csv(database):
    """
    Récupère les 1A du dump de la table wei_1a en csv
    """
    list_unA = []
    with  open(database) as csvfile:
        table = csv.DictReader(csvfile, delimiter=',')
        for people in table:
            info = {i: people[i] for i in INFOS_1A}
            if info["dept"] == 'A"2': # on aime pas les chimiste
                 info["dept"] = "A''2"
            # info["dept"] = "K"
            pos = [float(people["pos{}".format(i)]) for i in range(NB_BUS)]
            list_unA.append(UnA(info,pos))
    return list_unA

def get_bus_from_json(bus_json):
    """
    Récupère les infos sur les bus , stocker dans un json
    """
    list_bus = []
    with  open(bus_json, newline='') as jsonfile:
       buses = json.load(jsonfile)
       for bus in buses:
           nom  = bus["nom"]
           places_2a = bus["places_2a"]
           places = bus["places_totales"]
           size_equipe = bus["size_equipe"]
           pos  = bus["pos"]
           list_bus.append(Bus(nom,places ,places_2a,pos,size_equipe))
    return list_bus

# Pour que tout le monde soit content:
# le rang moyen du bus doit être le plus faible possible
# la note moyenne  du bus doit être la plus haute possible


def info_bus(bus,list_unA):
    rank_counter = [0,0,0,0,0,0,0,0]
    for unA in list_unA:
        print(unA.rank_bus)




def make_list(unA_file, bus_file):
    """
     - Fabrique les listes des 1A et des bus
     - puis génère les notes/classement pour chaque entité.
    """
    list_unA = get_1A_from_csv(unA_file)
    #list_unA = []
    #for x in list_unAB:
    #  if x.infos['genre'] == 'F':
    #     list_unA.append(x)
    list_bus = get_bus_from_json(bus_file)
    for unA in list_unA:
        unA.gen_score(list_bus)
        unA.gen_rank()
    for bus in list_bus:
        random.shuffle(list_unA)
        bus.gen_note(list_unA)
        bus.gen_rank()
    return list_bus, list_unA

def split_students(students, bus_places):
    """On remplis les bus au fur et à mesure """
    bus_ranks = {bus: bus.partial_rank(students) for bus in list_bus}
    students_ranks = {student: student.rank_bus[::] for student in students}
    return algorithms.resident_hospital(students_ranks, bus_ranks, bus_places)

def merge_bus(bus_matches_by_dpt):
    final_match = {bus: [] for bus in list_bus}
    for dpt in bus_matches_by_dpt:
        for bus in bus_matches_by_dpt[dpt]:
            match = bus_matches_by_dpt[dpt][bus]
            final_match[bus].extend(match)
    return final_match


def get_unA(idwei, students):
    for s in students:
        if int(s.infos["idwei"]) == idwei:
            return s

def hardcode(f, bus_by_name,students):
    """
    f : ficher de harcodage
    {"bus": [idwei,idwei, ...],...}
    """
    with open(f) as config:
        assigned =json.load(config)
    for b in assigned:
        bus = bus_by_name[b]
        bus.places_1a -= len(assigned[b])
        to_place=[]
        for idwei in assigned[b]:
             u = get_unA(idwei,students)
             if u != None:
                 to_place.append(u)
        bus.passengers.extend(to_place)
        for s in to_place:
            try:
                students.remove(s)
            except:
                print(s)

def make_repartition(list_bus,students):
    """
    Réparti les 1A dans les bus par département.
    - Les filles sont un département et passe en premier pour avoir le plus de choix
    -
    """
    list_dpt = set([s.infos["dept"] for s in students])
    N_etudiant = len(students)
    places_by_bus = {bus:bus.places_1a for bus in list_bus}
    N_places = sum([bus.places_1a for bus in list_bus])

    part_students = []
    bus_matches_by_dpt = dict()

    part_students = [f for f in students if f.infos['genre'] == 'F']
    n_filles = len(part_students)
    # pas de fille dans le bus aspique cette année.
    bus_places= {bus: int(bus.places_1a*n_filles/N_etudiant)+4 for bus in list_bus}

    # on commence par répartir les filles qui sont considéré comme un département
    bus_matches_by_dpt["filles"] = split_students(part_students, bus_places)

    for bus in bus_matches_by_dpt["filles"]:
        places_by_bus[bus] -= len(bus_matches_by_dpt["filles"][bus])

    # pour le reste des départements.
    for dpt in list_dpt:
        random.shuffle(list_bus)
        students = [s for s in students if s not in part_students]
        #part_students = [s for s in students if s.infos['dept'] == dpt]
        part_students = [s for s in students]
        places_hard_need = len(part_students)
        places_need =  int(places_hard_need*N_places/N_etudiant)
        bus_places = {bus: 0 for bus in list_bus}
        remain_places = {bus: places_by_bus[bus] for bus in list_bus}
        while places_need > 0:
            for bus in list_bus:
                if places_need > 0 and remain_places[bus] > 0:
                    bus_places[bus] += 1
                    remain_places[bus] -= 1
                    places_need -= 1
        # for bus in list_bus:
        #     if bus_places[bus] == 0 and remain_places[bus] > 0:
        #         bus_places[bus] += 1
        bus_matches_by_dpt[dpt] = split_students(part_students, bus_places)
        for bus in bus_matches_by_dpt[dpt]:
            places_by_bus[bus] -= len(bus_matches_by_dpt[dpt][bus])
    final_match = merge_bus(bus_matches_by_dpt)
    return final_match


def csv_bus_output(list_bus):
    """
    Fabrique un csv de la liste des participants 1A du bus, contenant les infos confidentielles
    """
    fieldnames = INFOS[1:] #tout sauf idwei
    for bus in match:
        with open("output/"+bus.nom+".csv",'w+',newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames,delimiter=',')
            writer.writeheader()
            for s in  bus.passengers:
                to_write = {i:s.infos[i] for i in fieldnames}
                # to_write.update({"rank_bus":s.rank_bus.index(bus),"fav_bus":s.rank_bus[0]})
                writer.writerow(to_write)

def csv_team_output(list_bus):
    """
    Fabrique un listing par équipe des 1As
    """
    for bus in list_bus:
        infos = [
                "nom",
                "prenom",
                "genre",
                "dept",
                "tel",
                ]
        fieldnames = infos + ["equipe"]
        with open("output/"+bus.nom+"team.csv","w+",newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames,delimiter=',')
            writer.writeheader()
            for t  in  bus.teams:
                for s in t:
                    to_write = {i:s.infos[i] for i in infos}
                    to_write["equipe"] = bus.teams.index(t)+1
                    writer.writerow(to_write)


if __name__=="__main__":
    #1 : on fabrique les classement
    list_bus, students = make_list("data/1ainfo.csv", "data/bus.json")

    bus_by_name={bus.nom:bus for bus in list_bus} # pour harcoder sans se prendre la tete
    print(len(students), [bus.places_1a for bus in list_bus], sum([bus.places_1a for bus in list_bus]))
    #2 : on hardcode ceux qui en ont en besoin
    hardcode("data/hardcode.json",bus_by_name,students)
    print([len(bus.passengers) for bus in list_bus])
    #3: mariage hongrois avec les restants
    #
    match = make_repartition(list_bus,students)
    for bus in match:
        bus.passengers.extend(match[bus])
        bus.make_teams()
        bus.stats()
        bus.print_teams()
        csv_bus_output(list_bus)
        csv_team_output(list_bus)

    #4: stats
