#!/usr/bin/env python3
from bus import NB_BUS
import numpy as np

INFOS = [
    "idwei","nom","prenom","genre","dept","tel","urgence_nom","urgence_tel","mail","infos"]

INFOS_2A = INFOS + [
    "idbde",
    "bus",
    "role"
]
INFOS_1A = INFOS + ["etranger"]+["pos{}".format(i) for i in range(NB_BUS)]


class People(object):
    def __init__(self, infos):
        self.infos = infos

    def __repr__(self):
        return repr("P_"+self.infos["nom"])

    def __str__(self):
        return "{} {}".format(self.infos["nom"].upper(), self.infos["prenom"])

class UnA(People):
    """un 1A est une gentille personne qui veux faire plein de rencontre dans le bon bus."""

    def __init__(self, infos, pos):
        People.__init__(self,infos)
        self.pos = np.array(pos)
        self.scores_bus = dict()
        self.rank_bus = []
    def __repr__(self):
        return repr("unA_"+self.infos["nom"])
    def __str__(self):
        return "{} {}".format(self.infos["nom"].upper(), self.infos["prenom"])

    def gen_score(self, list_bus):
        # norme euclidienne
        for bus in list_bus:
            self.scores_bus[bus] = np.linalg.norm(bus.pos-self.pos,ord=2)

    def gen_rank(self):
        """
        tri les bus en fonction du score obtenu
        """
        self.rank_bus = list(self.scores_bus.items()) #[(key,value)]
        self.rank_bus = sorted(self.rank_bus,key=lambda x:x[1])
        self.rank_bus = [r[0] for r in self.rank_bus]
