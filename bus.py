#! /usr/bin/env python3

NB_BUS=8

import numpy as np
import math

def dotproduct(v1, v2):
  return sum((a*b) for a, b in zip(v1, v2))

def length(v):
  return math.sqrt(dotproduct(v, v))

def angle(v1, v2):
  return math.acos(dotproduct(v1, v2) / (length(v1) * length(v2)))

class Bus(object):
    """Un bus contient des  équipes , remplis par des 1A en fonction de leur réponse à un questionnaire
     - contient les infos du bus
     - la position du barycentre du nuage de mot associé au bus.
    """
    def __init__(self, nom, places, places_2a,pos,size_equipe):
        self.nom = nom  # nom du bus
        self.pos = np.array(pos) # critère et pondération
        self.places = places
        self.places_2a = places_2a
        self.places_1a = places-places_2a
        self.rank_unA = []
        self.passengers = []
        self.old_passengers = []
        self.note = dict()  # note de tous les 1A par rapport à ce bus.
        self.size_team = [int(s) for s in size_equipe]
        self.teams = []

    def __repr__(self):
        ret = self.nom
        return"Bus_"+ret
    def __str__(self):
        return "Bus {}".format(self.nom)

    def __str__(self):
        return "Bus {}".format(self.nom)

    # def gen_note(self,list_unA):
    #     for unA in list_unA:
    #         score_unA = unA.scores_bus[self]
    #         rank_unA = unA.rank_bus.index(self)
    #         if rank_unA == NB_BUS-1:
    #             note_bus = 0
    #         else:
    #             note_bus = abs(angle(self.pos-unA.pos,unA.rank_bus[rank_unA+1].pos-unA.pos))*score_unA
    #         self.note[unA] = note_bus

    def gen_note(self,list_unA):
        for unA in list_unA:
            self.note[unA] = np.linalg.norm(self.pos-unA.pos,ord=2)

    def gen_rank(self):
        self.rank_unA = list(self.note.items()) # [(key,value),...]
        self.rank_unA = sorted(self.rank_unA,key=lambda x:x[1],reverse = False)
        self.rank_unA = [r[0] for r in self.rank_unA]

    def partial_rank(self, students):
        partial = [student for student in self.rank_unA if student in students]
        return partial

    def make_teams(self):
        n_chef_tot = sum(self.size_team)
        n_equipe = len(self.size_team)
        N = len(self.passengers)
        size_goal = [0]*len(self.size_team)
        for i in range(len(self.size_team)):
            size_goal[i] = int(self.size_team[i]*N/n_chef_tot)+1
        i = 0
        places_equipe=sum(size_goal)
        girls = [p for p in self.passengers
                 if p.infos["genre"] == 'F' or p.infos["genre"] == 'N']
        not_girls = [p for p in self.passengers
                 if p.infos["genre"] == 'M']

        self.teams=[[None]*s for s in size_goal]
        while girls :
            if None in self.teams[i]:
                self.teams[i].append(girls.pop())
                self.teams[i].remove(None)
                i +=1; i %= n_equipe;
        for i in range(len(self.size_team)):
            if len(self.teams[i])-self.teams[i].count(None) == 1:
                #Si une fille est seule on l'enlève.
                g_alone=self.teams[i].pop()
                self.teams[i].append(None)
                next_ind = (i+1)%n_equipe
                self.teams[next_ind].remove(None)
                self.teams[next_ind].append(g_alone)
        remain =True
        while not_girls and remain:
            if None in self.teams[i]:
                self.teams[i].append(not_girls.pop())
                self.teams[i].remove(None)
            i +=1; i %= n_equipe;
        #cleaned unused places
        for l in self.teams: 
            while None in l:
                l.remove(None)

    def stats(self):
        ranks = [unA.rank_bus.index(self) for unA in self.passengers]
        minval = min(ranks)
        maxval = max(ranks)
        mean   = sum(ranks)/len(ranks)
        medval = sorted(ranks)[int(len(ranks)/2)]
        print("==={}===".format(self.nom))
        print("min:{},max:{},mean:{:.2},med:{}".format(minval,maxval,mean,medval))

    def print_teams(self):
        for k,team in enumerate(self.teams):
            team_pretty = ["_".join([unA.infos["nom"],
                                     unA.infos["prenom"],
                                     unA.infos["genre"],
                                     unA.infos["dept"],
                                     str(unA.rank_bus.index(self))]) for unA in team]
            print(self.size_team[k],len(team),team_pretty)
